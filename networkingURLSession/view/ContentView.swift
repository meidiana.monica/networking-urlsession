//
//  ContentView.swift
//  networkingURLSession
//
//  Created by Meidiana Monica on 12/10/22.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var contentViewModel = ContentViewModel()
    let url = "https://swapi.dev/api/films"
    
    var body: some View {
        VStack {
            List {
                ForEach(contentViewModel.filmList, id: \.self) { list in
                    Text(list.title)
                }
            }
        }.onAppear {
            contentViewModel.fetchFilms(url: self.url)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
