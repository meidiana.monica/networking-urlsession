//
//  ContentModel.swift
//  networkingURLSession
//
//  Created by Meidiana Monica on 12/10/22.
//

import Foundation

struct DataFilm: Decodable {
    let count: Int
    let all: [Film]
    
    enum CodingKeys: String, CodingKey {
        case count
        case all = "results"
    }
}

struct Film: Decodable, Hashable {
    let id: Int
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case id = "episode_id"
        case title
    }
}
