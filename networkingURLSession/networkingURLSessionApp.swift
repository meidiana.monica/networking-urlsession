//
//  networkingURLSessionApp.swift
//  networkingURLSession
//
//  Created by Meidiana Monica on 12/10/22.
//

import SwiftUI

@main
struct networkingURLSessionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
