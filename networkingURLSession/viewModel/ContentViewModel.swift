//
//  ContentViewModel.swift
//  networkingURLSession
//
//  Created by Meidiana Monica on 12/10/22.
//

import Foundation
import Combine

class ContentViewModel: ObservableObject {
    @Published var filmList = [Film]()
    private var cancellables = Set<AnyCancellable>()
    
    func fetchFilms(url: String) {
        print("[ContentViewModel] fetchFilms URL \(url)")
        URLSession.shared
            .dataTaskPublisher(for: URL(string: url)!)
            .map(\.data)
            .decode(type: DataFilm.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: {completion in
                switch completion {
                    case let .failure(error):
                        print(error.localizedDescription)
                default:
                    break
                }
            }, receiveValue: { value in
                print("value \(value)")
                self.filmList = value.all
            })
            .store(in: &self.cancellables)
    }
    
//    func fetchFilms(url: String) {
//        print("[ContentViewModel] fetchFilms URL \(url)")
//        if let url = URL(string: url) {
//            print("[ContentViewModel] url \(url)")
//            //MARK: Create a URLSession
//            let session = URLSession(configuration: .default)
//
//            //MARK: give the session task
//            let task = session.dataTask(with: url, completionHandler: { data, response, error in
//                print(response)
//                let dataString = String(data: data!, encoding: .utf8)
//                print(dataString)
//
//                if error != nil {
//                    print(error)
//                    return
//                }
//
//                do {
//                    let json = try JSONDecoder().decode(DataFilm.self, from: data! )
//                    print("hasil json \(json)")
//                    self.filmList = json.all
//                } catch {
//                    print("error during json serialization : \(error.localizedDescription)")
//                }
//            })
//
//            //MARK: Start the task
//            task.resume()
//        }
//    }
}
